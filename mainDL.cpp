#include <iostream>
#include "doublelinked.h"
int main()
{
  DoubleLinkList mylist;
  mylist.insertFront(1);
  mylist.insertFront(0);
  mylist.insertBack(2);
  mylist.insertBack(3);
  mylist.insertBack(4);
  std::cout<<mylist.removeFront() << "\n";
  std::cout<<mylist.removeBack() << "\n";
  std::cout<<mylist.removeFront() << "\n";
  std::cout<<mylist.removeBack() << "\n";
  std::cout<<mylist.removeBack() << "\n";
  if(mylist.empty())
    std::cout<<"The list is empty\n";
  return 0;
}
