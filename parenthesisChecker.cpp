#include <iostream>
#include <cstring>
#include "stack.h"

bool checkPairs(int elem1, int elem2)
{
  if(elem1=='(' && elem2==')')
    return true;
  if(elem1=='[' && elem2==']')
    return true;
  if(elem1=='{' && elem2=='}')
    return true;
  return false;
}

int main(int argc, char* argv[])
{
  Stack mystack = Stack();
  int elem;
  char *expression=argv[1];
  
  for (int i=0; i< strlen(expression); i++)
    {
      if(expression[i] == '(' || expression[i] == '[' || expression[i] == '{')
	mystack.push(expression[i]);
      if(expression[i] == ')' || expression[i] == ']' || expression[i] == '}'){
	if(mystack.empty())
	  {
	    printf("Not balanced\n");
	    return 0;
	  }
	elem = mystack.pop();
	if(!checkPairs(elem, expression[i]))
	  {
	    printf("Not balanced\n");
	    return 0;
	  }
	
      }
    }
  if(!mystack.empty())
    {
      printf("Not balanced\n");
      return 0;
    }
  printf("Balanced\n");
  return 0;
}
