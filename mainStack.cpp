#include "stack.h"
#include <iostream>

int main(){
  int item;
  Stack mystack = Stack();
  
  mystack.push(0);
  mystack.push(1);
  mystack.push(2);
  while(!mystack.empty()){
    item = mystack.pop();
    std::cout<<item;
    std::cout<<",";
  }
  std::cout<<"\n";
}
