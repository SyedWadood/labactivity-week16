class Node{

private:
  int data;
  Node *next;
public:
  Node(int data, Node* next){
    this->data=data;
    this->next = next;
  }
  int getData(){return this->data;}
  Node *getNext(){return this->next;}
};

class Stack{

 private:
  Node * head;
 public:

  Stack(){this->head = nullptr; }
  
  ~Stack(){
  while (!empty())
    pop();
  }
  
  bool empty(){
    return this->head == nullptr;
  }

  void push(int item)
  {
    Node * newnode = new Node(item,this->head);
    this->head=newnode;

  }
  int pop(){
    int item = head->getData();
    Node *old_head = head;
    this->head = this->head->getNext();
    delete old_head;
    return item;
  }

};
