#include <iostream>

class Node{

private:
  int data;
  Node *next;
  Node *previous;
public:
  Node(int data, Node* next, Node *previous){
    this->data=data;
    this->next = next;
    this->previous=previous;
  }
  int getData(){return this->data;}
  Node *getNext(){return this->next;}
  Node *getPrevious(){return this->previous;}
  void setNext(Node * next){this->next = next;}
  void setPrevious(Node * previous){this->previous = previous;}

};

class DoubleLinkList{

private:
  Node *head=nullptr;
public:
  void insertFront(int item){
    Node * newnode = new Node(item,this->head,nullptr);
    if(!this->empty())
      this->head->setPrevious(newnode);
    this->head=newnode;
  }
  void insertBack(int item){
    Node *currentNode;
    if(this->empty())
      {
	insertFront(item);
	return;
      }
    currentNode = this->head;
    while(currentNode->getNext() != nullptr)
      currentNode = currentNode->getNext();
    Node * newnode = new Node(item,nullptr,currentNode);
    currentNode->setNext(newnode);
  }
  int removeFront(){
    int item;
    Node *currentNode;
    if(this->empty()){
      std::cout << "Error: List empty\n";
      return -1;
    }

    currentNode = this->head;
    item = this->head->getData();
    this->head = this->head->getNext();
    if(this->head != nullptr)
      this->head->setPrevious(nullptr);
    delete currentNode;
    return item;
  }
  int removeBack(){
    int item;
    Node *currentNode=this->head;
    Node *previousNode=nullptr;
    if(this->empty()){
      std::cout << "Error: List empty\n";
      return -1;
    }

    while(currentNode->getNext() != nullptr){
      previousNode = currentNode;
      currentNode = currentNode->getNext();      
    }
    if(previousNode == nullptr){
      this->head = nullptr;
    }
    else{
      previousNode->setNext(nullptr);
    }
    item = currentNode->getData();
    delete currentNode;
    return item;
  }
  bool empty(){
    return this->head == nullptr;
  }

};
